%define gtk4_version 4.12.5
%define libadwaita_version 1.4.2
%define upstream_version 3.1.6

Name:           clairvoyant
Version:        %upstream_version
Release:        alt1
Summary:        Get psychic answers from the magic 8-ball inspired fortune teller, Clairvoyant.
Group:          Games/Other
BuildArch:      x86_64

License:        GPL-3.0
URL:            https://github.com/cassidyjames/clairvoyant
Source0:        %{name}-%{version}.tar


BuildRequires(pre):  rpm-macros-meson
BuildRequires:  meson
BuildRequires:  intltool
BuildRequires:  glib2-devel
BuildRequires:  libgtk4-devel >= %{gtk4_version}
BuildRequires:  libportal-devel 
BuildRequires:  libadwaita-devel >= %{libadwaita_version}
BuildRequires:  desktop-file-utils

Requires:       libgtk4 >= %{gtk4_version}
Requires:       libadwaita >= %{libadwaita_version}

%description
Clairvoyant is a magic 8-ball inspired app for GNOME, offering insights into
life's questions. It encourages fun and curiosity with a simple interface,
adhering to the GNOME Code of Conduct. This ensures a respectful and engaging
experience for users seeking guidance or entertainment.

%prep
%setup -v


%build
%meson
%meson_build


%install
%meson_install
%find_lang %{name}
install -Dm644 LICENSE %{buildroot}%{_licensedir}/%{name}/LICENSE


%files -f %{name}.lang
%{_bindir}/com.github.cassidyjames.clairvoyant
%{_datadir}/applications/com.github.cassidyjames.clairvoyant.desktop
%{_datadir}/glib-2.0/schemas/com.github.cassidyjames.clairvoyant.gschema.xml
%{_datadir}/icons/hicolor/scalable/apps/com.github.cassidyjames.clairvoyant.svg
%{_datadir}/icons/hicolor/symbolic/apps/com.github.cassidyjames.clairvoyant-symbolic.svg
%{_datadir}/license/clairvoyant/LICENSE
%{_datadir}/locale/**
%{_datadir}/metainfo/com.github.cassidyjames.clairvoyant.metainfo.xml

%changelog
* Wed Mar 27 2024 Aleksandr A. Voyt <vojtaa@basealt.ru> 3.1.6-alt1
- First package version
